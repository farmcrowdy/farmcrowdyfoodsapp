import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/Engine/locationPojo.dart';
import 'package:farmcrowdyfoods/basket/cart.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

String backendUrl = '{YOUR_BACKEND_URL}';
//sk_test_c523b86c222a80e604fae0f515a7b493a37ec814
//pk_test_163449bfe6e18386d8dd7855be683940b67dcfab
//
//LIVE
//sk_live_2113359ff60b05d3ed85c1466507fd7c0e6378fb
//pk_live_ab176d53f8f92bc61d73e6d1b2c8e57bd521d157
String paystackPublicKey = 'pk_live_ab176d53f8f92bc61d73e6d1b2c8e57bd521d157';
String secretKey = 'sk_live_2113359ff60b05d3ed85c1466507fd7c0e6378fb';

class Pickup extends StatefulWidget {
  var amount;
  Pickup({Key key, this.amount}) : super(key: key);
  @override
  _Pickup createState() => _Pickup(amount);
}

class _Pickup extends State<Pickup> {
  var amount;

  _Pickup(this.amount);
  TextEditingController addressController = TextEditingController();
  int _mPickup = 0;
  var data;
  var responseData;
  var resData;
  var token;
  String name;
  String location;
  int id;
  int lid;
  var message;
  var userId;
  var email;
  String referenceNo;
  var itemId;
  var locNo;
  String phone;

  final _formKey = GlobalKey<FormState>();
  final _verticalSizeBox = const SizedBox(height: 20.0);
  final _horizontalSizeBox = const SizedBox(width: 10.0);
  var _border = new Container(
    width: double.infinity,
    height: 1.0,
    color: Colors.red,
  );

  int _radioValue = 0;
  CheckoutMethod _method;
  bool _inProgress = false;
  String _cardNumber;
  String _cvv;
  int _expiryMonth = 0;
  int _expiryYear = 0;

  void _valChange(int value) {
    setState(() {
      lid = value;
    });
  }

  // get the user default location
  void cancel() async {
    final response = await http.get(
      baseUrl + 'transaction/cancel/$itemId',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        print(responseData);
        Navigator.pushReplacementNamed(context, "/Cart");
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  // get the user default location
  void fetchDefaultLocation() async {
    final response = await http.get(
      baseUrl + 'user/default-location',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);

      setState(() {
        location = responseData[0]['name'];
        lid = responseData[0]['id'];
        print(responseData[0]['id']);
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  //Api to create transaction
  createTransaction(
      BuildContext context, int type, AnimationController controller) async {
    controller.forward();
    var body = {
      'user_id': userId.toString(),
      'status_id': '3',
      'pickup_location_id': addressController.text,
    };
    var header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };
    print(body);
    print(header);
    final response = await http.post(baseUrl + 'transaction/create',
        headers: header, body: body);
    if (response.statusCode == 200) {
      controller.reverse();

      //  Navigator.pop(context);

      setState(() {
        responseData = json.decode(response.body);
        message = responseData["message"];
        data = responseData["data"];
        referenceNo = responseData["data"]['reference'];
        itemId = responseData["data"]['id'];
        print(referenceNo);
        _handleCheckout(context);
      });
      refreshCart();
      print(responseData);
      print(body);
      print(message);
      Fluttertoast.showToast(msg: message);
    } else {
      controller.reverse();
      cancel();
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      Fluttertoast.showToast(
        msg: er,
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 3,
        backgroundColor: meatUpTheme,
      );
    }
  }

  // get all the cart data
  void refreshCart() async {
    final response = await http.get(
      baseUrl + 'basket/summary',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);

      setState(() {
        subTotal = responseData['data']['subtotal'];
        total = responseData['data']['total'];
        containerCharge = responseData['data']['container_charges'];
        vat = responseData['data']['delivery_charges'];
        basketSize = responseData['data']['basket_size'];
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  // get all the user data
  fetchData() async {
    final response = await http.get(
      baseUrl + 'user/profile',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];

      print(data);
      setState(() {
        name = data['name'];
        phone = data['phone_number'];
        addressController.text = data['address'];
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  // Get all locations
  Future<List<Location>> fetchLocation() async {
    final response = await http.get(
      baseUrl + 'location/all',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      resData = json.decode(response.body);
      data = resData['data'];
      print(data);
      final items = data.cast<Map<String, dynamic>>();
      List<Location> listOfUsers = items.map<Location>((json) {
        return Location.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      var sa = response.body;
      resData = json.decode(response.body);
      var er = resData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  // get the token for the user
  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      userId = prefs.getString('userId');
      name = prefs.getString('name');
      email = prefs.getString('email');
    });
  }

  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    setState(() {
      _method = CheckoutMethod.card;
    });
    PaystackPlugin.initialize(publicKey: paystackPublicKey);
    print(paystackPublicKey);
    Timer(Duration(milliseconds: 100), () {
      setState(() {
        fetchData();
      });
    });
    super.initState();
  }

  void _handleRadioValueChanged(int value) =>
      setState(() => _radioValue = value);

  _handleCheckout(BuildContext context) async {
    if (_method == null) {
      _showMessage('Select checkout method first');
      return;
    }

    if (_method != CheckoutMethod.card && _isLocal) {
      _showMessage('Select server initialization method at the top');
      return;
    }
    setState(() => _inProgress = true);

    Charge charge = Charge()
      ..amount = amount.round() // In base currency
      ..email = email
      ..reference = _getReference()
      ..putCustomField('Charged From $name', 'Eko Food Mart')
      ..card = _getCardFromUI();

    if (!_isLocal) {
      var accessCode = await _fetchAccessCodeFrmServer(_getReference());
      charge.accessCode = accessCode;
    } else {
      charge.reference = _getReference();
    }

    try {
      CheckoutResponse response = await PaystackPlugin.checkout(
        context,
        method: _method,
        charge: charge,
        fullscreen: true,
        logo: MyLogo(),
      );
      print('Response = $response');
      response.status == false
          ? cancel()
          : _sendAnalyticsEcomemrce();
      setState(() => _inProgress = false);
      _updateStatus(response.reference, '$response');
    } catch (e) {
      setState(() => _inProgress = false);
      _showMessage("Check console for error");
      rethrow;
    }
  }
// Per Analytics
  Future<void> _sendAnalyticsEcomemrce() async {
    Navigator.pushReplacementNamed(context, "/BottomNav");
    FirebaseAnalytics analytics = FirebaseAnalytics();

    await analytics.logEcommercePurchase(
      currency: 'NGN',
      value: amount,
      transactionId: referenceNo,
      startDate: DateTime.now().toString(),
    );
    print('logPackage Selected succeeded');

  }
  _startAfreshCharge() async {
    //_formKey.currentState.save();

    Charge charge = Charge();
    charge.card = _getCardFromUI();

    setState(() => _inProgress = true);

    if (_isLocal) {
      // Set transaction params directly in app (note that these params
      // are only used if an access_code is not set. In debug mode,
      // setting them after setting an access code would throw an exception

      charge
        ..amount = amount.round() // In base currency
        ..email = email
        ..reference = _getReference()
        ..putCustomField('Charged From $name', 'MeatHub');
      _chargeCard(charge);
      print(charge);
    } else {
      // Perform transaction/initialize on Paystack server to get an access code
      // documentation: https://developers.paystack.co/reference#initialize-a-transaction
      charge.accessCode = await _fetchAccessCodeFrmServer(_getReference());
      _chargeCard(charge);
      print(charge);
    }
  }

  _chargeCard(Charge charge) {
    // This is called only before requesting OTP
    // Save reference so you may send to server if error occurs with OTP
    handleBeforeValidate(Transaction transaction) {
      _updateStatus(transaction.reference, 'validating...');
    }

    handleOnError(Object e, Transaction transaction) {
      // If an access code has expired, simply ask your server for a new one
      // and restart the charge instead of displaying error
      if (e is ExpiredAccessCodeException) {
        _startAfreshCharge();
        _chargeCard(charge);
        return;
      }

      if (transaction.reference != null) {
        _verifyOnServer(transaction.reference);
      } else {
        setState(() => _inProgress = false);
        _updateStatus(transaction.reference, e.toString());
        // _showErrorDialog(message: e.toString() );
      }
    }

    // This is called only after transaction is successful
    handleOnSuccess(Transaction transaction) {
      _verifyOnServer(transaction.reference);
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => CustomDialog(
          title: "Success",
          description: 'Payment Successful,\n check your email for the receipt',
          buttonText: "Okay",
        ),
      );
    }

    {}

    PaystackPlugin.chargeCard(context,
        charge: charge,
        beforeValidate: (transaction) => handleBeforeValidate(transaction),
        onSuccess: (transaction) => handleOnSuccess(transaction),
        onError: (error, transaction) => handleOnError(error, transaction));
  }

  String _getReference() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS';
    } else {
      platform = 'Android';
    }
    print('helloRef$referenceNo');
    return '$referenceNo';
  }

  PaymentCard _getCardFromUI() {
    // Using just the must-required parameters.
    return PaymentCard(
      number: _cardNumber,
      cvc: _cvv,
      expiryMonth: _expiryMonth,
      expiryYear: _expiryYear,
    );
  }

  Widget _getPlatformButton(String string, Function() function) {
    // is still in progress
    Widget widget;
    if (Platform.isIOS) {
      widget = new CupertinoButton(
        onPressed: function,
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        color: CupertinoColors.activeBlue,
        child: new Text(
          string,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      );
    } else {
      widget = new RaisedButton(
        onPressed: function,
        color: Colors.blueAccent,
        textColor: Colors.white,
        padding: const EdgeInsets.symmetric(vertical: 13.0, horizontal: 10.0),
        child: new Text(
          string.toUpperCase(),
          style: const TextStyle(fontSize: 17.0),
        ),
      );
    }
    return widget;
  }

  Future<String> _fetchAccessCodeFrmServer(String reference) async {
    String url = '$backendUrl/new-access-code';
    String accessCode;
    try {
      print("Access code url = $url");
      http.Response response = await http.get(url);
      accessCode = response.body;
      print('Response for access code = $accessCode');
    } catch (e) {
      setState(() => _inProgress = false);
      _updateStatus(
          reference,
          'There was a problem getting a new access code form'
          ' the backend: $e');
    }

    return accessCode;
  }

  void _verifyOnServer(String reference) async {
    _updateStatus(reference, 'Verifying...');
    String url = 'https://api.paystack.co/transaction/verify/$reference';
    try {
      http.Response response = await http.get(url, headers: {
        'Authorization':
            'Bearer sk_live_2113359ff60b05d3ed85c1466507fd7c0e6378fb'
      });
      //http.Response response = await http.get(url,headers: {'Authorization':'Bearer sk_test_efe0e46a8a21eef111cadea486f68e928c9a81ea'});
      var body = response.body;
      _updateStatus(reference, body);
    } catch (e) {
      _updateStatus(
          reference,
          'There was a problem verifying %s on the backend: '
          '$reference $e');
    }
    setState(() => _inProgress = false);
  }

  _updateStatus(String reference, String message) {
    _showMessage('Reference: $reference \n\ Response: $message',
        const Duration(seconds: 7));
    print('update$message');
    String a = reference;
    print(a);
  }

  _showMessage(String message,
      [Duration duration = const Duration(seconds: 4)]) {
    print('msg$message');
    print('solo$message');
  }

  bool get _isLocal => _radioValue == 0;

  @override
  Widget build(BuildContext context) {
    //final percentage = _animationController.value * 100;
    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight = MediaQuery.of(context).size.height;
    print(amount);

    return Scaffold(
        backgroundColor: backgroundColor,
        body: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 60),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                height: 60,
                                width: 60,
                                //margin: EdgeInsets.only(top: 20),
                                // padding: EdgeInsets.only(top: 20,),
                                child: Image.asset(
                                  'images/back.png',
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            RichText(
                              text: TextSpan(
                                  text: 'Drop-Off Location',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                  children: [
                                    TextSpan(
                                        text: '',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400))
                                  ]),
                            )
                          ],
                        ),
                        Divider(),
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            'Address details',
                            style: TextStyle(
                                color: Colors.black45,
                                fontWeight: FontWeight.w500,
                                fontSize: 12),
                          ),
                        ),
                        Divider(
                          thickness: 6,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(10),
                              child: Text(name),
                            ),
                            Container(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        'Drop-Off Address',
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Container(
                                        height: 18,
                                        width: 18,
                                        child: Image.asset('images/pencil.png'),
                                      ),
                                    ],
                                  ),
                                  TextFormField(
                                    controller: addressController,
                                    maxLines: null,
                                    decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                      hintText:
                                          'Your preferred location for drop-off',
                                      hintStyle: TextStyle(
                                        color: Colors.black45,
                                      ),
                                      labelStyle: TextStyle(color: Colors.blue),
                                      border: new OutlineInputBorder(
                                        borderRadius:
                                            new BorderRadius.circular(5.0),
                                        borderSide: new BorderSide(),
                                      ),
                                    ),
                                    keyboardType: TextInputType.multiline,
                                    style: TextStyle(color: Colors.black),
                                    cursorColor: Colors.black,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.all(10),
                                child: Text(phone)),
                          ],
                        ),
                        Divider(
                          thickness: 6,
                        ),
                        Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'Your delivery arrives in two working days',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            )),
                        Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'Please note: All deliveries are within Lagos state',
                              style: TextStyle(fontWeight: FontWeight.w500, color: Colors.redAccent),
                            )),
                      ]),
                ),

                Container(
                  padding: EdgeInsets.all(20),
                  height: 80,
                  width: deviceWidth,
                  //margin: EdgeInsets.all(20),
                  child: ProgressButton(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: meatUpTheme,
                    strokeWidth: 2,
                    child: Text(
                      'Checkout Items',
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 12.0,
                        color: Colors.black,
                      ),
                    ),
                    onPressed: (AnimationController controller) {
                      createTransaction(
                        context,
                        1,
                        controller,
                      );
                    },
                  ),
                )
              ]),
        ),
     );
  }
}
//gotoPayment(BuildContext context, {var amount, String referenceNo , var amountShow}) {
//  Navigator.push(context, PageRouteBuilder(transitionDuration: Duration(milliseconds: 500),
//      pageBuilder: (_,__,___) => Payment(amount: amount, referenceNo: referenceNo, amountShow:amountShow )
//  ));
//}

var banks = ['Selectable', 'Bank', 'Card'];

CheckoutMethod _parseStringToMethod(String string) {
  CheckoutMethod method = CheckoutMethod.selectable;
  switch (string) {
    case 'Bank':
      method = CheckoutMethod.bank;
      break;
    case 'Card':
      method = CheckoutMethod.card;
      break;
  }
  return method;
}

const Color green = const Color(0xFF3db76d);
const Color lightBlue = const Color(0xFF34a5db);
const Color navyBlue = const Color(0xFF031b33);

class CustomDialog extends StatelessWidget {
  final String title, description, buttonText;
  final Image image;

  CustomDialog({
    @required this.title,
    @required this.description,
    @required this.buttonText,
    this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: Consts.avatarRadius + Consts.padding,
              bottom: Consts.padding,
              left: Consts.padding,
              right: Consts.padding,
            ),
            margin: EdgeInsets.only(top: Consts.avatarRadius),
            decoration: new BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(Consts.padding),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0,
                  offset: const Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 16.0),
                Text(
                  description,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(height: 24.0),
                Align(
                  alignment: Alignment.bottomRight,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(); // To close the dialog
                    },
                    child: GestureDetector(
                      onTap: () =>
                          Navigator.pushReplacementNamed(context, "/BottomNav"),
                      child: Text(buttonText),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: Consts.padding,
            right: Consts.padding,
            child: CircleAvatar(
              backgroundColor: Colors.blueAccent,
              backgroundImage: AssetImage('images/mark.png'),
              radius: Consts.avatarRadius,
            ),
          ),
          //...bottom card part,
          //...top circlular image part,
        ],
      ),
    );
  }
}

class Consts {
  Consts._();
  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}

class MyLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.black,
      ),
      alignment: Alignment.center,
      padding: EdgeInsets.all(10),
      child: CircleAvatar(
        backgroundColor: Colors.white,
        backgroundImage: AssetImage('images/logo.png'),
        radius: Consts.avatarRadius,
      ),
    );
  }
}
