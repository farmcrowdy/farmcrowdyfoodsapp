import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

class ProfileDetails extends StatefulWidget {
  @override
  _Pickup createState() => _Pickup();
}

class _Pickup extends State<ProfileDetails> {

  //Declare the variables
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();

  String gender;
  String location;
  var locations = List();
  var data;
  int _locationId;
  String message;
  Map<String, dynamic> responseData;
  var token;
  String name;
  String id ;



// get the token for the user
  void getToken() async {
    setCurrentScreen();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      name = prefs.getString('name');
    });
  }
//Analytics
  Future<void> setCurrentScreen() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setCurrentScreen(
      screenName: 'Update Profile Page',
      screenClassOverride: 'Update Profile',
    );
    print('setCartScreen succeeded');
  }
// get all the locations
// ignore: missing_return
  Future<String> _location() async {
    final response = await http
        .get(baseUrl + 'states/all', headers: {'Accept': 'application/json'});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        locations = responseData["data"];
        print(locations);
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }

  // get all the user data
  fetchData() async {
    final response = await http.get(
      baseUrl + 'user/profile',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data'];

      print(data);
      setState(() {
        nameController.text = data['name'];
        phoneNoController.text = data['phone_number'];
        emailController.text = data['email'];
        gender = data['gender'];
        cityController.text = data['city'];
        addressController.text = data['address'];
        _locationId = data['state_id'];
        print(_locationId);
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

// Update the user profile
 updateProfile() async {
    var body = {
      'name': nameController.text,
      'email': emailController.text,
      'gender': gender,
      'city': cityController.text,
      'address': addressController.text,
      'state_id': _locationId.toString(),
    };

    var header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    print(body);
    print(header);

    final response = await http.post(baseUrl + 'user/profile/update',
        headers: header, body: body);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      message = responseData["message"];
      data = responseData["data"];
      print(responseData);
      print(body);
      print(message);
      Fluttertoast.showToast(msg: message);
      Navigator.pop(context);
    }
    {
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      Fluttertoast.showToast(
        msg: er,
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 3,
        backgroundColor: meatUpTheme,
      );
    }
  }

  @override
  void initState() {
    //  TODO: implement initState
    super.initState();
    getToken();
    Timer(Duration(milliseconds: 200), () {
      _location();
    });

    Timer(Duration(milliseconds: 100), () {
      fetchData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 60),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 60,
                            width: 60,
                            //margin: EdgeInsets.only(top: 20),
                            // padding: EdgeInsets.only(top: 20,),
                            child: Image.asset(
                              'images/back.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Text(
                          'Your Details',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ]),
                      Divider(),
                      Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Name',
                                  style: TextStyle(
                                      color: Colors.black45,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                ),
                                TextFormField(
                                  //controller: emailController,
                                  decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                    hintText: 'Enter your full name here',
                                    hintStyle: TextStyle(
                                      color: Colors.black45,
                                    ),
                                    labelStyle: TextStyle(color: Colors.blue),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(5.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(color: Colors.black),
                                  cursorColor: Colors.black,
                                  controller: nameController,
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Phone Number',
                                  style: TextStyle(
                                      color: Colors.black45,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                ),
                                TextFormField(
                                  //controller: emailController,
                                  decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                    hintText: '08000101010',
                                    hintStyle: TextStyle(
                                      color: Colors.black45,
                                    ),
                                    labelStyle: TextStyle(color: Colors.blue),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(5.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(color: Colors.black),
                                  cursorColor: Colors.black,
                                  enabled: false,
                                  controller: phoneNoController,
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Email',
                                  style: TextStyle(
                                      color: Colors.black45,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                ),
                                TextFormField(
                                  //controller: emailController,
                                  decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                    hintText: 'name@example.com',
                                    hintStyle: TextStyle(
                                      color: Colors.black45,
                                    ),
                                    labelStyle: TextStyle(color: Colors.blue),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(5.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  keyboardType: TextInputType.emailAddress,
                                  style: TextStyle(color: Colors.black),
                                  cursorColor: Colors.black,
                                  controller: emailController,
                                  enabled: false,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Gender',
                                  style: TextStyle(
                                      color: Colors.black45,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                ),
                                Center(
                                  child: DropdownButton<String>(
                                    isExpanded: true,
                                    value: gender,
                                    onChanged: (String newValue) {
                                      setState(() {
                                        gender = newValue;
                                      });
                                    },
                                    items: <String>[
                                      'Select Gender',
                                      'Male',
                                      'Female',
                                      'Others',
                                    ].map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),

                          Container(
                            padding: EdgeInsets.all(20),
                            child:
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Drop-Off Address',
                                  style: TextStyle(
                                      color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12),
                                ),
                                TextFormField(
                                  controller: addressController,
                                  maxLines: null,
                                  decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                    hintText: 'Your preferred location for drop-off',
                                    hintStyle:TextStyle(
                                      color: Colors.black45,
                                    ) ,
                                    labelStyle: TextStyle(color: Colors.blue),
                                    border: new OutlineInputBorder(
                                      borderRadius: new BorderRadius.circular(5.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  keyboardType: TextInputType.multiline,
                                  style: TextStyle(color: Colors.black),
                                  cursorColor: Colors.black,
                                ),
                              ],
                            ),
                          ),

                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            padding: EdgeInsets.all(20),
                            child:
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'City',
                                  style: TextStyle(
                                      color: Colors.black45, fontWeight: FontWeight.bold, fontSize: 12),
                                ),
                                TextFormField(
                                  controller: cityController,
                                  decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                    hintText: 'Enter city for drop-off',
                                    hintStyle:TextStyle(
                                      color: Colors.black45,
                                    ) ,
                                    labelStyle: TextStyle(color: Colors.blue),
                                    border: new OutlineInputBorder(
                                      borderRadius: new BorderRadius.circular(5.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(color: Colors.black),
                                  cursorColor: Colors.black,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'State',
                                  style: TextStyle(
                                      color: Colors.black45,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                ),
                                Center(
                                  child: new DropdownButton(
                                    isExpanded: true,
                                    items: locations.map((item) {
                                      return new DropdownMenuItem(
                                        child: new Text(item['name']),
                                        value: item['id'],
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      setState(() {
                                        _locationId = newVal;
                                      });
                                    },
                                    value: _locationId,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    ]),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          margin: EdgeInsets.all(20),
          child: Material(
              borderRadius: BorderRadius.circular(7.0),
              color: meatUpTheme,
              elevation: 10.0,
              shadowColor: Colors.white70,
              child: MaterialButton(
                onPressed: () {
                  updateProfile();
                },
                child: Text(
                  'Update',
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 12.0,
                    color: Colors.black,
                  ),
                ),
              )),
        ));
  }
}
