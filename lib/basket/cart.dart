import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:farmcrowdyfoods/login/loginpageTwo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/basketOrderPojo.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/Payment/pickup.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

var basketSize;
var subTotal;
var total;
var containerCharge;
var vat;
var token;

String message;
var weight;
var res;

class Cart extends StatefulWidget {
  @override
  Carts createState() => Carts();
}

class Carts extends State<Cart> with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  final GlobalKey<Carts> _key = GlobalKey();

  var weight;
  var _quantity;
  var orderType;
  var id;
  var price;
  String name;
  var data;
  var totalCharge;
  Map<String, dynamic> responseData;
  bool noInternet = false;
  final oCcy = new NumberFormat("#,##0.00", "en_US");
  var er;
// get the token for the user
  void getToken() async {
    setCurrentScreen();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
      refresh = _fetch3();
    });
  }

  //Analytics
  Future<void> setCurrentScreen() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setCurrentScreen(
      screenName: 'Cart Page',
      screenClassOverride: 'Cart summary',
    );
    print('setCartScreen succeeded');
  }

  // get all the cart data
  void fetchData() async {
    final response = await http.get(
      baseUrl + 'basket/summary',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      print(responseData);
      setState(() {
        subTotal = responseData['data']['subtotal'];
        total = responseData['data']['total'];
        vat = responseData['data']['delivery_charges'];
        print(vat.runtimeType);
        basketSize = responseData['data']['basket_size'];
        totalCharge = (total * 0.015) + 100;
        if (totalCharge > 2000) {
          totalCharge = 2000;
        } else if (total == 0) {
          totalCharge = 0;
        }
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      er = responseData['message'];
      print('error$er');
      print(sa);
      setState(() {
        noInternet = true;
      });
      throw Exception('Failed to load internet');
    }
  }

  Future<List<Basket>> _fetch3() async {
    fetchData();
    final response = await http.get(
      baseUrl + 'basket/all',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      data = responseData['data']['orders'];
      print(data);
      final items = data.cast<Map<String, dynamic>>();
      List<Basket> listOfUsers = items.map<Basket>((json) {
        return Basket.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      setState(() {
        noInternet = true;
      });

      throw Exception('Failed to load internet');
    }
  }

  // Per Analytics
  Future<void> _sendAnalyticsBeginCheckout() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.logBeginCheckout(
      value: totalCharge,
      currency: 'NGN',
      //transactionId: 'test tx id',
      origin: 'Farmcrowdy Foods',
      destination: 'test destination',
      startDate: DateTime.now().toString(),
    );

    print('logPackage Selected succeeded');
  }

  Future<List<Basket>> refresh;

  Future<String> getData() async {
    fetchData();
    var response = await http.get(
      baseUrl + 'basket/all',
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    this.setState(() {
      res = json.decode(response.body);
      data = res['data']['orders'];
      print(data.toString());
      //print(res);
      setState(() {
//        _quantity = data['quantity'];
//        weight = data['weight'];
//        orderType = data['order_type_id'];
//        price = data['price'];
//        id = data['id'];
//        name = data['name'];
      });
      print(id);
    });
  }

  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );

    _animationController.addListener(() => setState(() {}));
    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void add() {
    setState(() {
      _quantity++;
    });
  }

  void minus() {
    setState(() {
      if (_quantity != 0) _quantity--;
    });
  }

  @override
  Widget build(BuildContext context) {
    final percentage = _animationController.value * 100;
    final deviceWidth = MediaQuery.of(context).size.width;

    final deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: backgroundColor,
        body: SingleChildScrollView(
          child: token == null
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(top: 60, left: 20),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
//                        GestureDetector(
//                          onTap: () {
//                            Navigator.pop(context);
//                          },
//                          child: Container(
//                            height: 60,
//                            width: 60,
//                            //margin: EdgeInsets.only(top: 20),
//                            // padding: EdgeInsets.only(top: 20,),
//                            child: Image.asset(
//                              'images/back.png',
//                              fit: BoxFit.fill,
//                            ),
//                          ),
//                        ),

                                    Text(
                                      'Basket',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                  ],
                                ),
                                Divider(
                                  thickness: 1,
                                ),
                                Center(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 150,
                                      ),
                                      Text(
                                        'Sign up or Log In',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      Text(
                                        'to view your basket',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(20),
                                        width: deviceWidth,
                                        child: Material(
                                            borderRadius:
                                                BorderRadius.circular(7.0),
                                            color: meatUpTheme,
                                            elevation: 10.0,
                                            shadowColor: Colors.white70,
                                            child: MaterialButton(
                                              onPressed: () {
                                                Navigator.pushNamed(
                                                    context, "/SignUp");
                                              },
                                              child: Text(
                                                'Get Started',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 12.0,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            )),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            new MaterialPageRoute(
                                                builder: (context) =>
                                                    new LoginTwo(
                                                        getToken: getToken)),
                                          );
                                          // Navigator.pushNamed(context, "/Login");
                                        },
                                        child: Text(
                                          'Log in',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: meatUpTheme,
                                            fontSize: 15,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ]))
                    ])
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 60, left: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
//                        GestureDetector(
//                          onTap: () {
//                            Navigator.pop(context);
//                          },
//                          child: Container(
//                            height: 60,
//                            width: 60,
//                            //margin: EdgeInsets.only(top: 20),
//                            // padding: EdgeInsets.only(top: 20,),
//                            child: Image.asset(
//                              'images/back.png',
//                              fit: BoxFit.fill,
//                            ),
//                          ),
//                        ),

                                Text(
                                  'Basket',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                    basketSize != null
                                        ? '($basketSize items)'
                                        : '',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400))
                              ],
                            ),
                            Divider(),
                            Container(
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              child: Text(
                                'Select drop-off location to checkout successfully',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 6,
                        color: Color(0xFFE7E7E7),
                        padding: EdgeInsets.all(2),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[],
                        ),
                      ),
                      Flex(direction: Axis.horizontal, children: [
                        Expanded(
                          child: FutureBuilder<List<Basket>>(
                              future: refresh,
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  return Column(children: <Widget>[
                                    SizedBox(
                                      height: 300,
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 40,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 24.0),
                                      child: LiquidLinearProgressIndicator(
                                        value: _animationController.value,
                                        backgroundColor: Colors.white,
                                        valueColor:
                                            AlwaysStoppedAnimation(meatUpTheme),
                                        borderRadius: 12.0,
                                        center: Text(
                                          "${percentage.toStringAsFixed(0)}%",
                                          style: TextStyle(
                                            color: Colors.redAccent,
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]);
                                }
                                //print(snapshot.data);
                                else if (snapshot.data.isNotEmpty) {
                                  return Container(
                                    height: deviceHeight / 2.5,
                                    child: ListView(
                                      //scrollDirection: Axis.vertical,
                                      shrinkWrap: true,
                                      children: snapshot.data
                                          .map(
                                            (feed) => InkWell(
                                              child: Container(
                                                  padding: EdgeInsets.all(5),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              right: 10.0,
                                                            ),
                                                            height: 120.0,
                                                            width: 110.0,
                                                            decoration:
                                                                BoxDecoration(
                                                              image: DecorationImage(
                                                                  image: CachedNetworkImageProvider(
                                                                      feed
                                                                          .image),
                                                                  fit: BoxFit
                                                                      .fill),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          12.0),
                                                            ),
                                                          ),
                                                          Flexible(
                                                              child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Container(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            10,
                                                                        right:
                                                                            10),
                                                                child: Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceBetween,
                                                                  children: <
                                                                      Widget>[
                                                                    Text(
                                                                      feed.name,
                                                                      style:
                                                                          TextStyle(
                                                                        fontWeight:
                                                                            FontWeight.w800,
                                                                        fontSize:
                                                                            15.0,
                                                                        color: Colors
                                                                            .black,
                                                                      ),
                                                                    ),
                                                                    feed.discountPrice !=
                                                                                0 ||
                                                                            feed.discountPrice !=
                                                                                null
                                                                        ? Text(
                                                                            feed.discountPrice != null
                                                                                ? '\u{20A6}${oCcy.format((feed.discountPrice))}'
                                                                                : '',
                                                                            style:
                                                                                TextStyle(
                                                                              fontWeight: FontWeight.w800,
                                                                              fontSize: 20.0,
                                                                              color: Colors.black,
                                                                            ),
                                                                          )
                                                                        : Text(
                                                                            feed.price != null
                                                                                ? '\u{20A6}${oCcy.format(feed.price)}'
                                                                                : '',
                                                                            style:
                                                                                TextStyle(
                                                                              fontWeight: FontWeight.w800,
                                                                              fontSize: 20.0,
                                                                              color: Colors.black,
                                                                            ),
                                                                          ),
                                                                  ],
                                                                ),
                                                              ),
//                            SizedBox(
//                              height: 5,
//                            ),
//                            Container(
//                                padding: EdgeInsets.only(left: 10, right: 10),
//                              child: feed.orderType == 1 ?
//                              Text('') :
//                              Text(
//                                  'Weight: ${feed.weight} kg'
//                              )
//                            ),
                                                              SizedBox(
                                                                height: 10,
                                                              ),
                                                              Container(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              10,
                                                                          right:
                                                                              10),
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceBetween,
                                                                    children: <
                                                                        Widget>[
                                                                      Text(
                                                                          'Quantity: ${feed.quantity} '),
                                                                      InkWell(
                                                                        onTap:
                                                                            () {
                                                                          //Update the user profile
                                                                          void
                                                                              delete() async {
                                                                            print('hello');
                                                                            var header =
                                                                                {
                                                                              'Accept': 'application/json',
                                                                              'Authorization': 'Bearer $token'
                                                                            };
                                                                            print(header);
                                                                            final response =
                                                                                await http.delete(
                                                                              baseUrl + 'order/delete/${feed.id}',
                                                                              headers: header,
                                                                            );
                                                                            if (response.statusCode ==
                                                                                200) {
                                                                              setState(() {
                                                                                refresh = _fetch3();
                                                                                setState(() {
                                                                                  basketSize = (basketSize - 1);
                                                                                });
                                                                                //fetchData();
                                                                              });
                                                                              print('jdhdd' + response.body);
                                                                              responseData = json.decode(response.body);
                                                                              message = responseData["message"];
                                                                              data = responseData["data"];
                                                                              print(responseData);
                                                                              print(message);
                                                                              Fluttertoast.showToast(msg: message);
                                                                            }
                                                                            {
                                                                              responseData = json.decode(response.body);
                                                                              var er = responseData['message'];
                                                                              print('error$er');
                                                                              Fluttertoast.showToast(
                                                                                msg: er,
                                                                                toastLength: Toast.LENGTH_LONG,
                                                                                timeInSecForIosWeb: 3,
                                                                                backgroundColor: meatUpTheme,
                                                                              );
                                                                            }
                                                                          }

                                                                          showDialog(
                                                                              context: context,
                                                                              builder: (context) {
                                                                                return AlertDialog(
                                                                                  title: Text(
                                                                                    "Delete item from cart",
                                                                                  ),
                                                                                  content: Text(
                                                                                    "Are you sure you want to delete this item from your cart?",
                                                                                  ),
                                                                                  actions: <Widget>[
                                                                                    FlatButton(
                                                                                      child: Text(
                                                                                        "Cancel",
                                                                                        style: TextStyle(color: Colors.white, fontSize: 16),
                                                                                      ),
                                                                                      onPressed: () => Navigator.pop(context),
                                                                                      color: Colors.grey,
                                                                                    ),
                                                                                    FlatButton(
                                                                                      child: Text(
                                                                                        "Delete",
                                                                                        style: TextStyle(color: meatUpTheme, fontSize: 16),
                                                                                      ),
                                                                                      onPressed: () {
                                                                                        Navigator.pop(context);
                                                                                        delete();
                                                                                      },
                                                                                    )
                                                                                  ],
                                                                                );
                                                                              });
                                                                        },
                                                                        child: Image
                                                                            .asset(
                                                                          'images/deleteicon.png',
                                                                          fit: BoxFit
                                                                              .fill,
                                                                          height:
                                                                              20,
                                                                          width:
                                                                              20,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  )),
                                                            ],
                                                          )),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      Divider(),
                                                    ],
                                                  )),
                                            ),
                                          )
                                          .toList(),
                                    ),
                                  );
                                } else if (snapshot.hasError) {
                                  return Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 100,
                                      ),
                                      Text(
                                        'No order yet',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                          "${'Once an order is placed all items can be found here'}"),
                                      SizedBox(
                                        height: 100,
                                      ),
                                    ],
                                  );
                                } else
                                  return Center(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(
                                          height: 100,
                                        ),
                                        Text(
                                          'No order yet',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Text(
                                                "${'Once an order is placed all items'}"),
                                            Text("${'can be found here'}"),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Container(
                                          margin: EdgeInsets.all(20),
                                          width: deviceWidth,
                                          child: Material(
                                              borderRadius:
                                                  BorderRadius.circular(7.0),
                                              color: meatUpTheme,
                                              elevation: 10.0,
                                              shadowColor: Colors.white70,
                                              child: MaterialButton(
                                                onPressed: () {
                                                  Navigator.pushNamed(
                                                      context, "/BottomNav");
                                                },
                                                child: Text(
                                                  'Place Order',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    fontSize: 12.0,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              )),
                                        )
                                      ],
                                    ),
                                  );
                              }),
                        ),
                      ]),
                      SizedBox(
                        height: 5,
                      ),
                      Divider(
                        thickness: 5,
                        color: Color(0xFFE7E7E7),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 20, top: 20),
                        child: Text(
                          'Order summary',
                          style: TextStyle(
                            fontWeight: FontWeight.w800,
                            fontSize: 12.0,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Sub-total',
                                style: TextStyle(
                                  // fontWeight: FontWeight.w800,
                                  fontSize: 16.0,
                                  color: Colors.black,
                                ),
                              ),
                              Row(children: <Widget>[
                                new Text(
                                  '\u{20A6}',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16),
                                ),
                                Text(
                                  subTotal != null ? oCcy.format(subTotal) : '',
                                  style: TextStyle(
                                    // fontWeight: FontWeight.w800,
                                    fontSize: 16.0,
                                    color: Colors.black,
                                  ),
                                ),
                              ]),
                            ],
                          )),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                          padding:
                              EdgeInsets.only(left: 20, right: 20, top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Delivery',
                                style: TextStyle(
                                  // fontWeight: FontWeight.w800,
                                  fontSize: 16.0,
                                  color: Colors.black,
                                ),
                              ),
                              Row(children: <Widget>[
                                new Text(
                                  '\u{20A6}',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16),
                                ),
                                Text(
                                  vat != null
                                      ? oCcy.format(int.parse(vat))
                                      : '0.00',
                                  style: TextStyle(
                                    // fontWeight: FontWeight.w800,
                                    fontSize: 16.0,
                                    color: Colors.black,
                                  ),
                                ),
                              ]),
                            ],
                          )),
                      Container(
                          padding:
                              EdgeInsets.only(left: 20, right: 20, top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Transaction charges',
                                style: TextStyle(
                                  // fontWeight: FontWeight.w800,
                                  fontSize: 16.0,
                                  color: Colors.black,
                                ),
                              ),
                              Row(children: <Widget>[
                                new Text(
                                  '\u{20A6}',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16),
                                ),
                                Text(
                                  totalCharge != null
                                      ? oCcy.format(totalCharge.round())
                                      : '',
                                  style: TextStyle(
                                    // fontWeight: FontWeight.w800,
                                    fontSize: 16.0,
                                    color: Colors.black,
                                  ),
                                ),
                              ]),
                            ],
                          )),
                      Divider(),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Total',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                  color: Colors.black,
                                ),
                              ),
                              Row(children: <Widget>[
                                new Text(
                                  '\u{20A6}',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                                Text(
                                  totalCharge != null
                                      ? oCcy.format(total + totalCharge.round())
                                      : '',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.0,
                                    color: Colors.black,
                                  ),
                                ),
                              ]),
                            ],
                          )),
                    ]),
        ),
        bottomNavigationBar: token == null
            ? Container(
                height: 0,
                width: 0,
              )
            : Container(
                margin: EdgeInsets.all(20),
                child: Material(
                    borderRadius: BorderRadius.circular(7.0),
                    color: meatUpTheme,
                    elevation: 10.0,
                    shadowColor: Colors.white70,
                    child: MaterialButton(
                      onPressed: () {
                        _sendAnalyticsBeginCheckout();
                        gotoPickup(
                          context,
                          amount: (total + totalCharge) * 100,
                        );
                        //  Navigator.pushNamed(context, "/Pickup");
                      },
                      child: Text(
                        'Select Drop-off Location',
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 12.0,
                          color: Colors.black,
                        ),
                      ),
                    )),
              ));
  }

  gotoPickup(BuildContext context, {var amount}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) => Pickup(amount: amount)));
  }
}
