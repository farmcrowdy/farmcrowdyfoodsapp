import 'dart:async';
import 'dart:convert';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

String universalToken;

class AnimatedSplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<AnimatedSplashScreen>
    with SingleTickerProviderStateMixin {
  var _visible = true;
  var token;

  AnimationController animationController;
  Animation<double> animation;
  String home = '/BottomNav';
  String login = '/BottomNav';

  var version;
  int currentVersion = 5;
  var data;
  Map<String, dynamic> responseData;

  launchGoogleStore() async {
    const url =
        "https://play.google.com/store/apps/details?id=com.farmcrowdyfoods";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  launchAppleStore() async {
    const url = "https://apps.apple.com/us/app/farmcrowdy-foods/id1512137851";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // Prompt user to update app
  void getVersion() async {
    final response = await http.get(
      baseUrl + 'version',
      headers: {'Accept': 'application/json'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        version = responseData['data']['version'];
        print('version' + version);

        if (int.parse(version) > currentVersion) {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text(
                    "New Update!",
                    style: TextStyle(color: Colors.black),
                  ),
                  content: Text(
                    'There is a new update, Kindly update your app',
                    style: TextStyle(color: Colors.black),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(15)),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(
                        "Update",
                        style: TextStyle(color: Colors.black),
                      ),
                      onPressed: () {
                        if (Theme.of(context).platform ==
                            TargetPlatform.android)
                          launchGoogleStore();
                        else if (Theme.of(context).platform ==
                            TargetPlatform.iOS)
                          launchAppleStore();
                        else
                          throw UnsupportedError(
                              'Only Android and iOS are supported.');
                        Navigator.pop(context);
                      },
                    ),
                    FlatButton(
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.black),
                      ),
                      onPressed: () {
                        startTime();
                        Navigator.pop(context);
                      },
                    )
                  ],
                );
              });
        } else {
          startTime();
        }
      });
    } else {
      startTime();
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      // throw Exception('Failed to load internet');
    }
  }

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    token = prefs.getString('token');
    setState(() {
      universalToken = token;
    });

    print(token);

    token == null
        ? Navigator.of(context).pushReplacementNamed(login)
        : Navigator.of(context).pushReplacementNamed(home);
  }

  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(
        vsync: this, duration: new Duration(seconds: 2));
    animation =
        new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      _visible = !_visible;
    });

    getVersion();
  }

  //
  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery.of(context).size.height;

    final deviceWidth = MediaQuery.of(context).size.width;
    return Stack(// <-- STACK AS THE SCAFFOLD PARENT
        children: [
      Container(
        height: deviceHeight,
        width: deviceWidth,
        child: Image.asset(
          'images/splashbackground.png',
          fit: BoxFit.fill,
        ),
      ),
      // TODO: implement build
      Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(30),
              height: 350,
              width: 350,
              child: new Image.asset(
                'images/splashlogo.png',
              ),
            ),
          ],
        ),
      ),
    ]);
  }
}
