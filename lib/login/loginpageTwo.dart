import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:farmcrowdyfoods/Engine/httpDetails.dart';
import 'package:farmcrowdyfoods/colorconst.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_indicator_button/progress_button.dart';

class LoginTwo extends StatefulWidget {
  final Function getToken;

  const LoginTwo({Key key, this.getToken}) : super(key: key);
  @override
  _Login createState() => _Login(getToken);
}

class _Login extends State<LoginTwo> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController numberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  Color btnColor;
  Map<String, dynamic> responseData;
  String message;
  var data;
  var token;
  String name;
  String phoneNo;
  int id;
  String email;
  SharedPreferences prefs;
  bool passwordVisible;
  var body;
  var getToken;

  _Login(this.getToken);
// Alert dialog
  _showErrorDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error Logging in"),
            content: Text(message),
            shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  //Http call
  login(BuildContext context, int type, AnimationController controller) async {
    controller.forward();
    prefs = await SharedPreferences.getInstance();

//
//      Pattern pattern =
//          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//      RegExp regex = new RegExp(pattern);
//      if (regex.hasMatch(numberController.text))
         body = {
          "email": numberController.text,
          "password": passwordController.text,
          "request_type": 'loginWithEmail'
        };

//      else{
//         body = {
//          "phone_number": numberController.text,
//          "password": passwordController.text,
//          "request_type": 'login'
//        };
//      }

    print(data);
    final response = await http.post(baseUrl + 'login',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        body: body);
    if (response.statusCode == 200) {
      controller.reverse();
      responseData = json.decode(response.body);
      message = responseData["message"];
      data = responseData["data"];
      token = responseData['data']['access_token'];
      name = responseData['data']['user']['name'];
      phoneNo = responseData['data']['user']['phone_number'];
      id = responseData['data']['user']['id'];
      email = responseData['data']['user']['email'];
      await prefs.setString('token', token);
      widget.getToken();
      Navigator.pop(context);
      //Navigator.pop(context);
      await prefs.setString('name', name);
      await prefs.setString('phoneNo', phoneNo);
      await prefs.setString('userId', '$id');
      await prefs.setString('email', '$email');
      print('token $token and $name');
      print(responseData);
      print(body);
      print(message);
      Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 3,
        backgroundColor: meatUpTheme,
      );
    } else {
      controller.reverse();
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      _showErrorDialog(message: er);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    if (numberController.text.isNotEmpty) {
      btnColor = meatUpTheme;
    } else {
      btnColor = Color(0xFFD4D4D4);
    }
    return Scaffold(
      backgroundColor: backgroundColor,
      key: _scaffoldKey,
      body: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          Stack(
            //alignment: Alignment.bottomLeft,
            children: <Widget>[
              Container(
                child: Image.asset(
                  'images/signin.png',
                  width: deviceWidth,
                  height: 239,
                  fit: BoxFit.fill,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: 80,
                  width: 60,
                  margin: EdgeInsets.only(top: 20),
                   padding: EdgeInsets.only(top: 20,),
                  child: Image.asset(
                    'images/back.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ),

              Container(
                padding: EdgeInsets.only(top: 180, left: 20),
                height: 240,
                width: 170,
                child: Image.asset(
                  'images/foodlogo.png',
                  fit: BoxFit.fill,
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                child: Text(
                  'Log in to your account',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Email Address',
                      style: TextStyle(
                          color: Colors.black45,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),
                    TextFormField(
                      controller: numberController,
                     // maxLength: 11,
                      decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                        hintText: '',
                        hintStyle: TextStyle(
                          color: Colors.black45,
                        ),
                        labelStyle: TextStyle(color: Colors.blue),
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                    ),
                  ],
                ),
              ),

              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Password',
                      style: TextStyle(
                          color: Colors.black45,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),
                    TextFormField(
                      controller: passwordController,
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                          icon: Icon(
                            // Based on passwordVisible state choose the icon
                            passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            // Update the state i.e. toogle the state of passwordVisible variable
                            setState(() {
                              passwordVisible = !passwordVisible;
                            });
                          },
                        ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                        hintText: '******',
                        hintStyle: TextStyle(
                          color: Colors.black45,
                        ),
                        labelStyle: TextStyle(color: Colors.blue),
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      obscureText: passwordVisible,
                      keyboardType: TextInputType.visiblePassword,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(20),
                width: deviceWidth,
                height: 100,
                child: ProgressButton(
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  color: btnColor,
                  strokeWidth: 2,
                  child: Text(
                    "Log in to your Account",
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 12.0,
                      color: Colors.black,
                    ),
                  ),
                  onPressed: (AnimationController controller) {
                    login(
                      context,
                      1,
                      controller,
                    );
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Forgot password? ",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.normal),
                    ),
                    new GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, "/ResetPassword");
                      },
                      child: new Text(
                        "Reset it here",
                        style: TextStyle(
                          color: meatUpTheme2,
                          fontWeight: FontWeight.w600,
                          fontSize: 13,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Have no account? ",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.normal),
                    ),
                    new GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, "/SignUp");
                      },
                      child: new Text(
                        "Sign up here",
                        style: TextStyle(
                          color: meatUpTheme2,
                          fontWeight: FontWeight.w600,
                          fontSize: 13,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      )),
    );
  }
}
