class Meats {

  String  name;
  String  description;
  int id;
  int packageId;
  String createdAt;
  String updatedAt;
  String image;
  String shortDescription;
  int price;
  int discount;
  int calDiscount;

  Meats({
    this.name,
    this.description,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.packageId,
    this.image,
    this.shortDescription,
    this.price,
    this.discount,
    this.calDiscount
  });

  Meats.fromJson(Map<String, dynamic> doc):
        name =  doc['name'],
        description =  doc['description'],
        id =  doc['id'],
  updatedAt = doc['updated_at'],
  packageId = doc['order_type_id'],
  createdAt = doc['created_at'],
  image=  doc['image'],
  shortDescription = doc['short_description'],
        price=  doc['price'],
  discount = doc['discount'];
}
