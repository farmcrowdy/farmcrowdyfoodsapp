

class OrdersDetails {

int  id;
int  packageId;
int userId;
int weight;
int quantity;
int paymentId;
int statusId;
String createdAt;
String updatedAt;
int amount;
String name;
var image;




OrdersDetails({
this.id,
this.userId,
this.statusId,
this.createdAt,
this.updatedAt,
this.weight,
this.paymentId,
this.packageId,
  this.amount,
  this.name,
  this.quantity,
  this.image
});

OrdersDetails.fromJson(Map<String, dynamic> doc):

id =  doc['id'],
userId =  doc['user_id'],
statusId =  doc['status_id'],
updatedAt = doc['updated_at'],
weight = doc['weight'],
createdAt = doc['created_at'],
paymentId = doc['payment_id'],
packageId = doc['package_id'],
      amount = doc['amount'],
quantity = doc['quantity'],
name = doc['name'],
image= doc['image'];

}
